import { Entity } from "../models/entity";

export class Repository<T extends Entity> {
    private entities: T[] = []
    private nextId: number = 1

    public add(entity: T): void {
        entity.id = this.nextId++
        this.entities.push(entity)
    }

    public find(id: number): T {
        let result = this.entities.find(e => e.id == id)
        if (result === undefined) {
            throw new Error(`Не найден элемент с id = ${id}`)
        }
        return result
    }

    public getAll(): T[] {
        return this.entities
    }

    public getAllWhere(condition: (entity: T) => boolean): T[] {
        return this.entities.filter(condition)
    }

    public moveUp(id: number): void {
        let i = this.entities.findIndex(e => e.id == id)
        if (i > 0) {
            this.swapEntities(i, i - 1)
        }
    }

    public moveDown(id: number): void {
        let i = this.entities.findIndex(e => e.id == id)
        if (i < this.entities.length - 1) {
            this.swapEntities(i, i + 1)
        }
    }

    public delete(id: number): void {
        this.entities = this.entities.filter(e => e.id != id)
    }

    private swapEntities(index1: number, index2: number): void {
        let temp = this.entities[index1]
        this.entities[index1] = this.entities[index2]
        this.entities[index2] = temp
    }
}