import { Injectable } from "@angular/core";
import { Repository } from "../utils/repository";
import { Account } from "../models/input/account";
import { Spend } from "../models/input/spend";
import { Transfer } from "../models/input/transfer";
import { Debt } from "../models/output/debt";

@Injectable({
    providedIn: 'root'
})
export class DebtService {

    public calculateAccountDebts(
        accounts: Repository<Account>,
        spends: Repository<Spend>,
        transfers: Repository<Transfer>
    ): Debt[] {
        let debts: Debt[] = []
        for (let debtor of accounts.getAll()) {
            // No one owes anything to himself
            for (let creditor of accounts.getAllWhere(a => a.id != debtor.id)) {
                let debtSum = 0
                // Spends made by debtor from creditor's account
                debtSum += spends
                    .getAllWhere(s => s.expectedAccountId == debtor.id && s.realAccountId == creditor.id)
                    .reduce((sum, s) => sum + s.sum, 0)
                // Transfers from debtor to creditor
                debtSum -= transfers
                    .getAllWhere(t => t.accountFromId == debtor.id && t.accountToId == creditor.id)
                    .reduce((sum, s) => sum + s.sum, 0)
                if (debtSum > 0) {
                    debts.push(new Debt(debtSum, debtor.id, creditor.id))
                }
            }
        }
        return debts
    }
}