import { Injectable } from '@angular/core';
import { Difference } from '../models/output/difference';
import { Repository } from '../utils/repository';
import { Account } from '../models/input/account';
import { Spend } from '../models/input/spend';
import { Transfer } from '../models/input/transfer';

@Injectable({
  providedIn: 'root'
})
export class DifferenceService {

  public calculateAccountDifferences(
    accounts: Repository<Account>,
    spends: Repository<Spend>,
    transfers: Repository<Transfer>
  ): Difference[] {
    let differences: Difference[] = []
    for (let account of accounts.getAll()) {
      let diffSum = 0
      // Spends from account
      diffSum -= spends
        .getAllWhere(s => s.realAccountId == account.id)
        .reduce((sum, s) => sum + s.sum, 0)
      // Transfers from account
      diffSum -= transfers
        .getAllWhere(t => t.accountFromId == account.id)
        .reduce((sum, t) => sum + t.sum, 0)
      // Transfers to account
      diffSum += transfers
        .getAllWhere(t => t.accountToId == account.id)
        .reduce((sum, t) => sum + t.sum, 0)
      differences.push(new Difference(account.id, diffSum))
    }
    return differences
  }

}
