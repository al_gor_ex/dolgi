export class Difference {
    constructor(
        public accountId: number,
        public sum: number
    ) {}
}