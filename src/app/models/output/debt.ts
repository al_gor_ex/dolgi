export class Debt {
    constructor(
        public sum: number,
        public accountFromId: number,
        public accountToId: number
    ) {}
}