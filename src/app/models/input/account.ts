import { Entity } from "../entity";

export class Account extends Entity {
    constructor(
        public name: string,
        public color: string
    ) {
        super()
    }
}