import { Entity } from "../entity";

export class Transfer extends Entity {
    constructor(
        public sum: number,
        public accountFromId: number,
        public accountToId: number
    ) {
        super()
    }
}