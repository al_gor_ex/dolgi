import { Entity } from "../entity";

export class Spend extends Entity {
    constructor(
        public description: string,
        public sum: number,
        public realAccountId: number,
        public expectedAccountId: number
    ) {
        super()
    }
}