import { Component } from '@angular/core';
import { Repository } from './utils/repository';
import { Account } from './models/input/account';
import { Spend } from './models/input/spend';
import { Transfer } from './models/input/transfer';
import { Difference } from './models/output/difference';
import { Debt } from './models/output/debt';
import { DifferenceService } from './services/difference.service';
import { DebtService } from './services/debt.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // INPUT
  public accounts: Repository<Account>
  public spends: Repository<Spend>
  public transfers: Repository<Transfer>
  // RESULT
  public differences: Difference[] | null
  public debts: Debt[] | null

  constructor(
    private diffService: DifferenceService,
    private debtService: DebtService
  ) {
    this.accounts = new Repository<Account>()
    this.spends = new Repository<Spend>()
    this.transfers = new Repository<Transfer>()
    this.differences = null
    this.debts = null
  }

  // ACCOUNTS

  public addNewAccountRow(): void {
    this.accounts.add(new Account('', '#000000'))
  }

  public moveAccountRowUp(id: number): void {
    this.accounts.moveUp(id)
  }

  public moveAccountRowDown(id: number): void {
    this.accounts.moveDown(id)
  }

  public deleteAccountRow(id: number): void {
    if (this.hasUsages(id)) {
      alert('Нельзя удалить счёт, на который есть ссылки')
      return
    }
    this.accounts.delete(id)
  }

  // SPENDS

  public onSpendRealAccountChange($event: Event, spendId: number): void {
    this.spends.find(spendId).realAccountId = Number(($event.target as HTMLSelectElement).value)
  }

  public onSpendExpectedAccountChange($event: Event, spendId: number): void {
    this.spends.find(spendId).expectedAccountId = Number(($event.target as HTMLSelectElement).value)
  }

  public addNewSpendRow(): void {
    let mockAccountId = this.accounts.getAll()[0].id
    this.spends.add(new Spend('', 0, mockAccountId, mockAccountId))
  }

  public moveSpendRowUp(id: number): void {
    this.spends.moveUp(id)
  }

  public moveSpendRowDown(id: number): void {
    this.spends.moveDown(id)
  }

  public deleteSpendRow(id: number): void {
    this.spends.delete(id)
  }

  // TRANSFERS

  public onTransferAccountFromChange($event: Event, transferId: number): void {
    this.transfers.find(transferId).accountFromId = Number(($event.target as HTMLSelectElement).value)
  }

  public onTransferAccountToChange($event: Event, transferId: number): void {
    this.transfers.find(transferId).accountToId = Number(($event.target as HTMLSelectElement).value)
  }

  public addNewTransferRow(): void {
    let mockAccountId = this.accounts.getAll()[0].id
    this.transfers.add(new Transfer(0, mockAccountId, mockAccountId))
  }

  public moveTransferRowUp(id: number): void {
    this.transfers.moveUp(id)
  }

  public moveTransferRowDown(id: number): void {
    this.transfers.moveDown(id)
  }

  public deleteTransferRow(id: number): void {
    this.transfers.delete(id)
  }

  // RESULT CALCULATION

  public calculateResults(): void {
    this.differences = this.diffService.calculateAccountDifferences(this.accounts, this.spends, this.transfers)
    this.debts = this.debtService.calculateAccountDebts(this.accounts, this.spends, this.transfers)
  }

  public roundMoney(sum: number): number {
    return Math.round(sum * 100) / 100
  }

  private hasUsages(accountId: number): boolean {
    if (this.spends.getAllWhere(
      s => s.realAccountId == accountId || s.expectedAccountId == accountId
    ).length > 0) {
      return true;
    }
    if (this.transfers.getAllWhere(
      t => t.accountFromId == accountId || t.accountToId == accountId
    ).length > 0) {
      return true;
    }
    return false;
  }
}
